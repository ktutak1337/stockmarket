﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using StockMarket.Api;
using System.Net.Http;
using System.Text;

namespace StockMarket.Tests.EndToEnd.Helpers
{
    public abstract class FakeServer
    {
        public static readonly TestServer Server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
        public static readonly HttpClient Client = Server.CreateClient();

        public FakeServer() { }

        public static StringContent GetPayload(object data)
        {
            var json = JsonConvert.SerializeObject(data);

            return new StringContent(json, Encoding.UTF8, "application/json");
        }
    }
}
