﻿using NUnit.Framework;
using StockMarket.Tests.EndToEnd.Helpers;
using System.Net;
using System.Threading.Tasks;
using StockMarket.Infrastructure.Commands.Users;
using System;

namespace StockMarket.Tests.EndToEnd.Controllers
{
    [TestFixture]
    public class UsersControllerTests
    {
        [Test]
        public async Task GetAsync_GivenInvalidEmailUser_ShouldNotExist()
        {
            //-------------------- Arrange --------------------

            var email = "user333@domain.net";

            //---------------------- Act ----------------------

            var response = await FakeServer.Client.GetAsync($"users/{email}");

            //-------------------- Assert ---------------------

            Assert.AreEqual(response.StatusCode, HttpStatusCode.NotFound);
        }

        [Test]
        public async Task PostAsync_GivenUniqueEmailUser_shouldBeCreated()
        {
            //-------------------- Arrange --------------------

            var command = new CreateUser
            {
                Id = Guid.NewGuid(),
                Email = "test1@domain.com",
                Username = "test1",
                Password = "secret123",
                Role = "investor"
            };

            var payload = FakeServer.GetPayload(command);

            //---------------------- Act ----------------------

            var response = await FakeServer.Client.PostAsync("users", payload);

            //-------------------- Assert ---------------------

            Assert.AreEqual(response.StatusCode, HttpStatusCode.Created);
            Assert.AreEqual(response.Headers.Location.ToString(), $"users/{command.Email}");

            var user = await UsersHelpers.GetUserAsync(command.Email);

            Assert.AreEqual(user.Email, command.Email);
        }
    }
}
