﻿using NUnit.Framework;
using StockMarket.Infrastructure.Commands.Users;
using StockMarket.Tests.EndToEnd.Helpers;
using System.Net;
using System.Threading.Tasks;

namespace StockMarket.Tests.EndToEnd.Controllers
{
    [TestFixture]
    public class AccountControllerTests : FakeServer
    {
        [Test]
        public async Task PutAsync_GivenValidCurrentAndNewPassword_ItShouldBeChanged()
        {
            //-------------------- Arrange --------------------

            var command = new ChangeUserPassword
            {
                CurrentPassword = "secret123",
                NewPassword = "secret666"
            };

            var payload = GetPayload(command);

            //---------------------- Act ----------------------

            var response = await Client.PutAsync("account/password", payload);

            //-------------------- Assert ---------------------

            Assert.AreEqual(response.StatusCode, HttpStatusCode.NoContent);
        }
    }
}
