using System;

namespace StockMarket.Infrastructure.DTO
{
    public class UserDto
    {
        #region Properties
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }

        #endregion 
    }
}
