﻿using StockMarket.Infrastructure.Commands.Interfaces;
using System;

namespace StockMarket.Infrastructure.Commands.Users
{
    public class CreateUser : ICommand
    {
        #region Properties
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }

        #endregion
    }
}
