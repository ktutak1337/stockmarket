﻿using StockMarket.Infrastructure.Commands.Interfaces;

namespace StockMarket.Infrastructure.Commands.Users
{
    public class ChangeUserPassword : ICommand
    {
        #region Properties
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        
        #endregion
    }
}
