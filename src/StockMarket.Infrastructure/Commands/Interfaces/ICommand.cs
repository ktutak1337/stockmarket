﻿namespace StockMarket.Infrastructure.Commands.Interfaces
{
    /// <summary>
    /// Marker Interface.
    /// </summary>
    public interface ICommand { }
}
