﻿using Autofac;
using StockMarket.Infrastructure.Commands.Interfaces;
using System;
using System.Threading.Tasks;

namespace StockMarket.Infrastructure.Commands
{
    public class CommandDispatcher : ICommandDispatcher
    {
        #region Fields
        private readonly IComponentContext _context;

        #endregion

        #region Constructors
        public CommandDispatcher(IComponentContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods
        public async Task DispatchAsync<T>(T command) where T : ICommand
        {
            if(command == null)
            {
                throw new ArgumentNullException(nameof(command), $"Command {typeof(T).Name} can not be null.");

                var handler = _context.Resolve<ICommandHandler<T>>();
                await handler.HandleAsync(command);
            }
        }

        #endregion
    }
}
