﻿using AutoMapper;
using StockMarket.Core.Domain;
using StockMarket.Infrastructure.DTO;

namespace StockMarket.Infrastructure.Mappers
{
    /// <summary>
    /// The class provides initialize configuration of the Automapper library.
    /// </summary>
    public static class AutoMapperConfig
    {
        /// <summary>
        /// Creates a MapperConfiguration instance and initialize configuration via the constructor.
        /// </summary>
        /// <returns>Returns the IMapper instance.</returns>
        public static IMapper Initialize()
            => new MapperConfiguration(config =>
            {
                config.CreateMap<User, UserDto>();
            })
            .CreateMapper();
    }
}
