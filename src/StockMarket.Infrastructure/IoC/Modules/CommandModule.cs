﻿using Autofac;
using StockMarket.Infrastructure.Commands;
using StockMarket.Infrastructure.Commands.Interfaces;
using System.Reflection;

namespace StockMarket.Infrastructure.IoC.Modules
{
    /// <summary>
    /// The class containing the registration of all command modules in the application.
    /// </summary>
    public class CommandModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(CommandModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .AsClosedTypesOf(typeof(ICommandHandler<>))
                .InstancePerLifetimeScope();

            builder.RegisterType<CommandDispatcher>()
                .As<ICommandDispatcher>()
                .InstancePerLifetimeScope();
        }
    }
}
