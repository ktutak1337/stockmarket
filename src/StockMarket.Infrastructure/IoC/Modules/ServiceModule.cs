﻿using Autofac;
using StockMarket.Infrastructure.Services.Interfaces;
using System.Reflection;

namespace StockMarket.Infrastructure.IoC.Modules
{
    /// <summary>
    /// The class containing the registration of all services modules in the application.
    /// </summary>
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ServiceModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IService>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
