﻿using Autofac;
using StockMarket.Core.Repositories.Interfaces;
using System.Reflection;

namespace StockMarket.Infrastructure.IoC.Modules
{
    /// <summary>
    /// The class containing the registration of all repository modules in the application.
    /// </summary>
    public class RepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(RepositoryModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IRepository>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
