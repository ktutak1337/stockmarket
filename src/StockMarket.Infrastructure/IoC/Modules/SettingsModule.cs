﻿using Autofac;
using Microsoft.Extensions.Configuration;
using StockMarket.Infrastructure.Extensions;
using StockMarket.Infrastructure.Settings;

namespace StockMarket.Infrastructure.IoC.Modules
{
    /// <summary>
    /// The class containing the registration of all settings modules in the application.
    /// </summary>
    public class SettingsModule : Autofac.Module
    {
        #region Fields
        private readonly IConfiguration _configuration;

        #endregion

        #region Constructors
        /// <summary>
        /// The SettingsModule class constructor.
        /// </summary>
        /// <param name="configuration"> The represents a set of key/value application configuration properties.</param>
        public SettingsModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        #endregion

        #region Methods
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(_configuration.GetSettings<JwtSettings>())
                   .SingleInstance();
        }

        #endregion
    }
}
