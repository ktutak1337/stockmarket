﻿using Autofac;
using Microsoft.Extensions.Configuration;
using StockMarket.Infrastructure.Mappers;

namespace StockMarket.Infrastructure.IoC.Modules
{
    /// <summary>
    /// The class containing the registration of all IoC modules in the application.
    /// </summary>
    public class ContainerModule : Autofac.Module
    {
        #region Fields
        private readonly IConfiguration _configuration;

        #endregion

        #region Constructors
        /// <summary>
        /// The ContainerModule class constructor.
        /// </summary>
        /// <param name="configuration"> The represents a set of key/value application configuration properties.</param>
        public ContainerModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        #endregion

        #region Methods
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(AutoMapperConfig.Initialize()).SingleInstance();
            builder.RegisterModule<RepositoryModule>();
            builder.RegisterModule<ServiceModule>();
            builder.RegisterModule<CommandModule>();
            builder.RegisterModule(new SettingsModule(_configuration));
        }

        #endregion
    }
}
