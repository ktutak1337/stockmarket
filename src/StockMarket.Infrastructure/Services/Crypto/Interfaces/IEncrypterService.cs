﻿using StockMarket.Infrastructure.Services.Interfaces;

namespace StockMarket.Infrastructure.Services.Crypto.Interfaces
{
    public interface IEncrypterService : IService
    {
        string GetSalt(string value);
        string GetHash(string value, string salt);
    }
}
