﻿using StockMarket.Infrastructure.Extensions;
using System;
using System.Security.Cryptography;

namespace StockMarket.Infrastructure.Services.Crypto
{
    public class EncrypterService
    {
        #region Fields
        private static readonly int DeriveBytesIterationsCount = 10000;
        private static readonly int SaltSize = 40;

        #endregion

        #region Methods
        public string GetSalt(string value)
        {
            if (value.IsEmpty()) throw new ArgumentException("Value can not be empty.", nameof(value));
            
            var saltBytes = new byte[SaltSize];
            RandomNumberGenerator.Create()
                .GetBytes(saltBytes);

            return Convert.ToBase64String(saltBytes);
        }

        public string GetHash(string value, string salt)
        {
            if (value.IsEmpty()) throw new ArgumentException("Value can not be empty.", nameof(value));
            
            if (salt.IsEmpty()) throw new ArgumentException("Salt can not be empty.", nameof(value));

            var pbkdf2 = new Rfc2898DeriveBytes(value, GetBytes(salt), DeriveBytesIterationsCount);

            return Convert.ToBase64String(pbkdf2.GetBytes(SaltSize));
        }

        private static byte[] GetBytes(string value)
        {
            var bytes = new byte[value.Length * sizeof(char)];
            Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);

            return bytes;
        }

        #endregion
    }
}
