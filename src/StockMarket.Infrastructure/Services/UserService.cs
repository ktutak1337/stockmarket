using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StockMarket.Core.Domain;
using StockMarket.Core.Repositories;
using StockMarket.Infrastructure.DTO;
using StockMarket.Infrastructure.Services.Interfaces;
using AutoMapper;

namespace StockMarket.Infrastructure.Services
{
    public class UserService : IUserService
    {
        #region Fields
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        #endregion

        #region Constructors
        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        #endregion

        #region Methods
        public Task<IEnumerable<UserDto>> BrowseAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<UserDto> GetAsync(string email)
        {
            var user = await _userRepository.GetAsync(email);

            return _mapper.Map<User, UserDto>(user);
        }

        public Task LoginAsync(string email, string password)
        {
            throw new NotImplementedException();
        }

        public async Task RegisterAsync(Guid userId, string email, string username, string password, string role)
        {
            var user = await _userRepository.GetAsync(email);

            if(user != null)
            {
                throw new Exception($"User with email: '{email}' already exists.");
            }
            // TODO: Generate salt
            user = new User(userId, email, username, role, "password", "salt");
            await _userRepository.AddAsync(user);
        }

        #endregion
    }
}
