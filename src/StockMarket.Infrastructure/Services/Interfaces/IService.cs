﻿namespace StockMarket.Infrastructure.Services.Interfaces
{
    /// <summary>
    /// Marker interface.
    /// </summary>
    public interface IService { }
}
