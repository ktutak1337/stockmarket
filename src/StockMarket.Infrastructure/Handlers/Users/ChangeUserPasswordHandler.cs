﻿using StockMarket.Infrastructure.Commands.Interfaces;
using StockMarket.Infrastructure.Commands.Users;
using System.Threading.Tasks;

namespace StockMarket.Infrastructure.Handlers.Users
{
    public class ChangeUserPasswordHandler : ICommandHandler<ChangeUserPassword>
    {
        public async Task HandleAsync(ChangeUserPassword command)
        {
            await Task.CompletedTask;
        }
    }
}
