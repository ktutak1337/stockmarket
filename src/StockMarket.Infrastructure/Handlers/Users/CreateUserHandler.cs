﻿using StockMarket.Infrastructure.Commands.Interfaces;
using StockMarket.Infrastructure.Commands.Users;
using StockMarket.Infrastructure.Services.Interfaces;
using System.Threading.Tasks;

namespace StockMarket.Infrastructure.Handlers.Users
{
    public class CreateUserHandler : ICommandHandler<CreateUser>
    {
        #region Fields
        private readonly IUserService _userService;

        #endregion

        #region Constructors
        public CreateUserHandler(IUserService userService)
        {
            _userService = userService;
        }

        #endregion

        #region Methods
        public async Task HandleAsync(CreateUser command)
        {
            await _userService.RegisterAsync(command.Id, command.Email, command.Username, command.Password, command.Role);
        }

        #endregion
    }
}
