using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StockMarket.Core.Domain;
using StockMarket.Core.Repositories;

namespace StockMarket.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
		#region Fields
        private static readonly ISet<User> _users = new HashSet<User>
		{
			new User(Guid.NewGuid(), "user1@email.com", "user1", "investor", "Secret123", "salt"),
			new User(Guid.NewGuid(), "user2@email.com", "user2", "investor", "Secret123", "salt"),
			new User(Guid.NewGuid(), "user3@email.com", "user3", "investor", "Secret123", "salt")
		};

		#endregion

		#region Methods
		public async Task<User> GetAsync(Guid id)
			=> await Task.FromResult(_users.SingleOrDefault(x => x.Id == id));

		public async Task<User> GetAsync(string email)
			=> await Task.FromResult(_users.SingleOrDefault(x => x.Email == email.ToLowerInvariant()));

		public async Task<IEnumerable<User>> GetAllAsync()
			=> await Task.FromResult(_users);

		public async Task AddAsync(User user)
		{
			_users.Add(user);
			await Task.CompletedTask;
		}

		public async Task RemoveAsync(Guid id)
		{
			var user = await GetAsync(id);
			_users.Remove(user);
			await Task.CompletedTask;
		}

		public async Task UpdateAsync(User user)
		{
			await Task.CompletedTask;
		}

		#endregion
    }
}
