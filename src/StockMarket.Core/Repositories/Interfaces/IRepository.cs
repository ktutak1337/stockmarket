﻿namespace StockMarket.Core.Repositories.Interfaces
{
    /// <summary>
    /// Marker interface.
    /// </summary>
    public interface IRepository { }
}
