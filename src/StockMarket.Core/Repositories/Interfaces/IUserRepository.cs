using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StockMarket.Core.Domain;
using StockMarket.Core.Repositories.Interfaces;

namespace StockMarket.Core.Repositories
{
    public interface IUserRepository : IRepository
    {
        #region Methods
        Task<User> GetAsync(Guid id); 
        Task<User> GetAsync(string email);
        Task<IEnumerable<User>> GetAllAsync();
        Task AddAsync(User user);
        Task UpdateAsync(User user);
        Task RemoveAsync(Guid id);

        #endregion
    }
}
