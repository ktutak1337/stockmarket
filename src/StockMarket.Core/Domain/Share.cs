namespace StockMarket.Core.Domain
{
    public class Share
    {
       #region Properties 
       public string Name { get; protected set; }
       public string Symbol { get; protected set; }
       public double Price { get; protected set; }
       
       #endregion
    }
}