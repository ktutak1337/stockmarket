namespace StockMarket.Core.Domain
{
    public class Wallet
    {
       #region Properties 
       public string Security { get; protected set; }
       public double UnitPrice { get; protected set; }
       public double Amount { get; protected set; }
       public double Value { get; protected set; }

       #endregion
    }
}