﻿using Microsoft.AspNetCore.Mvc;
using StockMarket.Infrastructure.Commands.Interfaces;

namespace StockMarket.Api.Controllers
{
    [Route("[controller]")]
    public abstract class ApiControllerBase : Controller
    {
        protected readonly ICommandDispatcher CommandDispatcher;

        protected ApiControllerBase(ICommandDispatcher commandDispatcher)
        {
            CommandDispatcher = commandDispatcher;
        }
    }
}
