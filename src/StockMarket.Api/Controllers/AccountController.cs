﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using StockMarket.Infrastructure.Commands.Interfaces;
using StockMarket.Infrastructure.Commands.Users;

namespace StockMarket.Api.Controllers
{
    [ApiController]
    public class AccountController : ApiControllerBase
    {
        protected AccountController(ICommandDispatcher commandDispatcher) : base(commandDispatcher) { }

        // PUT api/values/5
        [HttpPut]
        [Route("password")]
        public async Task<IActionResult> Put([FromBody] ChangeUserPassword command)
        {
            await CommandDispatcher.DispatchAsync(command);

            return NoContent();
        }
    }
}
