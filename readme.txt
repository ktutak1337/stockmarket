Required steps to run the application:

1. Download and install the .NET SDK (Software Development Kit).  
	Obtain: .NET Core 2.1 - https://www.microsoft.com/net/download

2. Download the Visual Studio or Visual Studio Code:
	IDE: https://visualstudio.microsoft.com	

3.  First, you will need to restore the packages:	
	 command: dotnet restore

4.  Then you can either run from source:
	 command: dotnet run